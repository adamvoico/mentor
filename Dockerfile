FROM alpine:edge

#RUN mkdir /voicoadmin

RUN  apk update
RUN apk add unzip nodejs npm composer php7 php7-session php7-xmlwriter php7-dom php7-bcmath php7-ctype php7-fileinfo php7-json php7-mbstring php7-openssl php7-pdo_mysql php7-pdo_odbc php7-pdo php7-pdo_pgsql php7-pdo_sqlite php7-tokenizer php7-xml
RUN composer global require laravel/installer
#RUN /root/.composer/vendor/bin/laravel new mentor
COPY template.zip /
RUN unzip /template.zip
WORKDIR /template
RUN composer install
RUN npm audit fix
RUN npm install
COPY start.sh /template/
EXPOSE 8000
ENTRYPOINT ["/template/start.sh"]
#RUN /usr/bin/php /voicoadmin/mentor/artisan serve

#CMD ["/usr/bin/php" "/voicoadmin/mentor/artisan" "serve"]
#RUN mkdir /voicoadmin/template/
#COPY template/* /voicoadmin/template/
#RUN npm --prefix ./template install
#RUN ping -c 3000000 google.com

